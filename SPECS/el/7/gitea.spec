# Autoset our release number.
%define rpm_release_number %(date +"%%y%%m%%d%%H%%M")
# latest package versions
%define go_latest %(curl -sL https://go.dev/VERSION?m=text)
%define node_latest %(curl -sL https://raw.githubusercontent.com/nodesource/distributions/master/rpm/setup_current.x | grep 'NODEREPO' | cut -d'=' -f2 | head -1 )
%define gitea_latest %(curl -sL https://api.github.com/repos/go-gitea/gitea/releases/latest | grep tag_name | awk '{ print $2 }' | rev | cut -c 3- | rev | cut -c 3- )
%define __uname_m %(uname -m)

%undefine _debugsource_packages

Name:           gitea
Version:        %{gitea_latest}
Release:        %{rpm_release_number}
Summary:        Git with a cup of tea, painless self-hosted git service
License:        MIT
Group:          Development/Other
URL:            https://gitea.io/
Source0:        https://github.com/go-gitea/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
Source10:       %{name}.service
Source11:       %{name}.service.d.conf
Source12:       %{name}.ini
Source13:       %{name}.env
Patch0:         make-version.patch
Requires:       git-core, openssl
BuildRequires:  golang, git, make, jq, pam-devel, pkgconfig(sqlite3), nodejs, npm

%description
The goal of this project is to make the easiest, fastest, and most painless way
of setting up a self-hosted Git service. It is similar to GitHub, Bitbucket,
and Gitlab. Gitea is a fork of Gogs.

# Prepare sources for building package
%prep
# Get Latest Node Version
dnf install "https://rpm.nodesource.com/%{node_latest}/el/7/%{__uname_m}/nodesource-release-el7-1.noarch.rpm"
curl "https://dl.google.com/go/%{go_latest}.linux-amd64.tar.gz" -o go-latest.linux-amd64.tar.gz
rm -rf /usr/local/go && tar -C /usr/local -xzf go-latest.linux-amd64.tar.gz

# Auto patchs
%autosetup -p1

%build
export PATH=$PATH:/usr/local/go/bin
export GOPATH="`pwd`/.godeps"
TAGS="bindata sqlite sqlite_unlock_notify pam" make VERSION=%version generate all

# Pre-install: Add our User
%pre
useradd %{name} -d /opt/%{name} -s /sbin/nologin

%install

# Make Directories
# /{opt,etc}/{name}
${__mkdir_p} %{buildroot}/{opt,etc}/%{name}
# /usr/share/{name}
${__mkdir_p} %{buildroot}%{_docdir}/%{name}
# /var/log/{name}
${__mkdir_p} %{buildroot}%{_var}/log/%{name}
# /etc/pki/{name}/{token,private,misc,certs}
${__mkdir_p} %{buildroot}%{_sysconfdir}/pki/%{name}/{token,private,misc,certs}
# /etc/systemd/system/{name}.service.d
${__mkdir_p} %{buildroot}%{_sysconfdir}/systemd/system/%{name}.service.d

# install {name} /usr/bin/{name}
${__install} -Dm 0755 %{name} %{buildroot}%{_bindir}/%{name}
# install {name}.service /etc/systemd/system/{name}.service
${__install} -Dm 0640 %{SOURCE10} %{buildroot}%{_sysconfdir}/systemd/system/%{name}.service
# install {name}.service.d.conf /etc/systemd/system/{name}.service.d/port.conf
${__install} -Dm 0640 %{SOURCE11} %{buildroot}%{_sysconfdir}/systemd/system/%{name}.service.d/port.conf
# install {name}.ini /etc/{name}/app.ini
${__install} -Dm 0660 %{SOURCE12} %{buildroot}%{_sysconfdir}/%{name}/app.ini
# install {name}.env /etc/{name}/app.env
${__install} -Dm 0660 %{SOURCE13} %{buildroot}%{_sysconfdir}/%{name}/app.env
# install "custom/conf/app.example.ini" /usr/share/{name}/default-app.ini
${__install} -Dm 0644 "custom/conf/app.example.ini" %{buildroot}%{_docdir}/%{name}/default-app.ini

%post

# Post install: Enable {name} Service
systemctl %{name}.service enable
# Post install: Start {name} Service
systemctl %{name}.service start

%{_bindir}/%{name} cert -ca=true -duration=8760h0m0s -host=$(hostname)

# Creates a Random Private Key on first install. ( See 'SECRET_KEY_URI' in gitea.ini )
if [ ! -f %{_sysconfdir}/pki/%{name}/private/secret.key ]; then
  echo "$(openssl rand -base64 32)" > %{_sysconfdir}/pki/%{name}/private/secret.key
  %{__chmod} 440 %{_sysconfdir}/pki/%{name}/private/secret.key
  %{__chown} root:%{name} %{_sysconfdir}/pki/%{name}/private/secret.key
fi

%files
%{_bindir}/%{name}
%dir %attr(0700,%{name},%{name}) /opt/%{name}
%dir %attr(0700,%{name},%{name}) %{_var}/log/%{name}
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/pki/%{name}/{token,private,misc,certs}
%config(noreplace) %attr(0660,root,%{name}) %{_sysconfdir}/%{name}/app.ini
%config(noreplace) %attr(0660,root,%{name}) %{_sysconfdir}/%{name}/app.env
%config(noreplace) %attr(0660,root,%{name}) %{_sysconfdir}/systemd/system/%{name}.service.d/port.conf
%{_sysconfdir}/systemd/system/%{name}.service
%{_docdir}/%{name}/default-app.ini
%doc *.md

%preun
# Pre uninstall: Stop {name} Service
systemctl %{name}.service stop
# Pre uninstall: Disable {name} Service
systemctl %{name}.service disable

%postun
userdel %{name}

%changelog
* Tue Dec 06 2022 Llewellyn Curran <melinko2003@gmail.com> - 1.17.3
- Variable clean up 
- Moving from /srv to /opt
* Sun Oct 24 2021 Wei-Lun Chao <bluebat@member.fsf.org> - 1.15.6
- Rebuilt for Fedora