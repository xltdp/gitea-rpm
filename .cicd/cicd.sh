#!/bin/bash
# Simple Cicd.sh to perform CICD operations for this RPM inside the Vagrant container.

### APP Env
export PATH=$PATH:/usr/local/bin

sudo dnf install rpm-build rpmdevtools yum-utils jq rpmlint -y

# RPM Build Root Structure
sudo rm -rf /opt/rpmbuild
sudo mkdir -p /opt/rpmbuild/{BUILD,BUILDROOT}

if [[ -d "/vagrant/RPMS/x86_64" ]]; then
	mkdir -p /vagrant/RPMS/archive
	cp /vagrant/RPMS/x86_64/* /vagrant/RPMS/archive/
	rm -rf /vagrant/RPMS/x86_64
fi

# Copy in our Sources
sudo cp -a /vagrant/SOURCES /opt/rpmbuild/

# Head to the Build Directory 
cd /vagrant/

### Lint Spec File
sudo rpmlint ${1}

### Build 

# Yum/dnf install spec file dependencies.
sudo yum-builddep -y ${1}
# RPMBuild our Spec
sudo rpmbuild --verbose --undefine=_disable_source_fetch --define "_topdir /opt/rpmbuild" --define "_srcrpmdir /vagrant/SRPMS" --define "_rpmdir /vagrant/RPMS" --define "_specdir /vagrant/SPECS" -bb ${1}

### Unit Test
sudo rpmlint --verbose --file /vagrant/gitea.rpmlint RPMS/x86_64/*.rpm

### Deploy
sudo dnf install -y RPMS/x86_64/*.rpm

### Integrations Tests

### Store