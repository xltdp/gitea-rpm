# gitea-rpm

RPM builds of Gitea  

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)[![Copr build status](https://copr.fedorainfracloud.org/coprs/melinko2003/gitea/package/gitea/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/melinko2003/gitea/package/gitea/)
  
Quick Enable
```#> dnf copr enable melinko2003/gitea```

Linting
```gitea-rpm $ rpmlint src/gitea.spec ```
  
# Vagrant Builds
Currently Virtualbox is supported. `vagrant up` to build the latest rpm. You will be prompted to install the plugin `vagrant-vbguest`. After build completes you should see an RPM: 
```
$ tree RPMS/
RPMS/
└── x86_64
    └── gitea-1.17.3-2212192010.x86_64.rpm

1 directory, 1 file
```

